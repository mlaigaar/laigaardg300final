﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnManager : MonoBehaviour {

    //variable used for creating endless platforms
    public static int platformCount = 1;
	public GameObject[] platforms;
	//controls the distance that the platforms spawn from each other
	public float horizontalMin = 15f;
	public float horizontalMax = 17f;
	public float verticalMin = -6f;
	public float verticalMax = 4f;

	private Vector2 originPosition;
    private int index;

	// Use this for initialization
	void Start () {
		//sets first position the same position of the spawn manager
		originPosition = transform.position;

	}

    void Update () {
        if (platformCount < 50)
            Spawn();
    }

	void Spawn(){
		//randomizes the location of the new platforms, using originPosition as and randomPosition to offset each other
		Vector2 randomPosition = originPosition + new Vector2 (Random.Range(horizontalMin, horizontalMax), Random.Range(verticalMin, verticalMax));
        index = Random.Range(0, platforms.Length);
        Instantiate (platforms[index], randomPosition, Quaternion.identity);
		originPosition = randomPosition;
        platformCount += 1;

	}

}
