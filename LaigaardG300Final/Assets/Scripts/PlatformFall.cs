﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformFall : MonoBehaviour {

    [HideInInspector] public bool destroyed = false;
	public float fallDelay = 0.4f;

	private Rigidbody2D rb2d;

	// Use this for initialization
	void Awake () {
		rb2d = GetComponent<Rigidbody2D> ();
	}

	//called when the player lands on a platform
	void OnCollisionEnter2D (Collision2D other)
	{
		//if the player has the "Player" tag, call the Fall function after fallDelay seconds
		if (other.gameObject.CompareTag ("Player"))
			if (Random.value < 0.3f)
				Invoke ("Fall", fallDelay);
        //destroy the platform long after it is passed to save space
        Destroy(gameObject, 5f);
        SpawnManager.platformCount -= 1;

	}

	void Fall()
	{
        //tells the rigidbody to be affected by the collision, and fall
        rb2d.isKinematic = false;
	}
}
