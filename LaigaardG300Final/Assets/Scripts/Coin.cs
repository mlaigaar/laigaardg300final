﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour {

	// Use this for initialization
	void Start () {

	}

    void OnTriggerEnter2D (Collider2D other)
    {
        // If the coin collides with an object tagged "Player", it will be destroyed
        if (other.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject);
        }
    }
}
