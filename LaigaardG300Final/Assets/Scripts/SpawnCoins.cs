﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnCoins : MonoBehaviour {

    public Transform[] coinSpawns;
    public GameObject coin;

	// Use this for initialization
	void Start () {
        Spawn();
	}
	
    void Spawn()
    {
        //if i is less than the number of objects in coinSpawns enter loop
        for (int i = 0; i < coinSpawns.Length; i++)
        {
            //creates a ranadom variable of 0 or 1, if 1 then a coin will be instantiate
            //creates a 50% chance of spawning a coin at all coinSpawn locations
            int coinFlip = Random.Range(0, 2);
            if (coinFlip > 0)
                Instantiate(coin, coinSpawns[i].position, Quaternion.identity);
        }
    }

}
