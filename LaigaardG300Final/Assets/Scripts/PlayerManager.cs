﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : PhysicsObject
{
    
    public float maxSpeed = 10;
    public float jumpTakeOffSpeed = 11;

    private SpriteRenderer spriteRenderer;
    private Animator animator;

    // Use this for initialization
    void Awake()
    {
        spriteRenderer = GetComponent<SpriteRenderer>();
        animator = GetComponent<Animator>();
    }

    protected override void ComputeVelocity()
    {
        Vector2 move = Vector2.zero;
        move.x = Input.GetAxis("Horizontal");

        if (Input.GetButtonDown("Jump") && grounded)
        {
            velocity.y = jumpTakeOffSpeed;
        }
        else if (Input.GetButtonUp("Jump"))
        {
            if (velocity.y > 0)
            {
                velocity.y = velocity.y * 0.5f;
            }
        }
        //was not able to fully implement the attack function
        else if (Input.GetKeyDown(KeyCode.J))
        {
            animator.SetBool("Attack", true);
        }
        else if (Input.GetKeyUp(KeyCode.J))
        {
            animator.SetBool("Attack", false);
        }
        //unsure why this is starting the player facing the wrong way
        bool flipSprite = (spriteRenderer.flipX ? (move.x > 0.01f) : (move.x < 0.01f));
        if (flipSprite)
        {
            spriteRenderer.flipX = !spriteRenderer.flipX;
        }

        animator.SetBool("Idle", grounded);
        animator.SetFloat("velocityX", Mathf.Abs(velocity.x) / maxSpeed);

        targetVelocity = move * maxSpeed;
    }
}