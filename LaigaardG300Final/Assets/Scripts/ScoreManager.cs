﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreManager : MonoBehaviour {

    private int playerScore;
    private int topScore = 0;

    public Text scoreText;
    public Text HighScore;


	// Use this for initialization
	void Start () {
        topScore = PlayerPrefs.GetInt("HighScore", 0);
        playerScore = 0;
        SetScore();

	}
	
    void OnTriggerEnter2D(Collider2D other)
    {
        // If the coin collides with an object tagged "Player", it will be destroyed
        if (other.gameObject.CompareTag("Pickup"))
        {
            playerScore++;
            SetScore();
        }
    }

    void SetScore()
    {
        scoreText.text = "Current Score: " + playerScore.ToString();

        HighScore.text = "High Score: " + topScore.ToString();
    }

    void Update()
	{
        if (DeathTrigger.isDead == true)
        {
            if (playerScore > topScore)
            {
                PlayerPrefs.SetInt("HighScore", playerScore);
            }
        }
	}



}
